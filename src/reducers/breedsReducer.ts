import { FetchBreedsAction, SelectBreedAction } from '../actions/breedActions';
import { BreedState } from '../types/breed';
import ActionTypes from '../actionTypes';

const initialState = {
  list: [],
  selected: null,
};

export default (
  state: BreedState = initialState,
  action: FetchBreedsAction | SelectBreedAction
): BreedState => {
  switch (action.type) {
    case ActionTypes.fetchBreeds:
      return { ...state, list: action.payload };
    case ActionTypes.selectBreed:
      return { ...state, selected: action.payload };
    default:
      return state;
  }
};
