import { combineReducers } from 'redux';
import { BreedState } from '../types/breed';
import { CatState } from '../types/cat';
import catsReducer from './catsReducer';
import breedsReducer from './breedsReducer';

export interface StoreState {
  breeds: BreedState;
  cats: CatState;
}

export default combineReducers<StoreState>({
  breeds: breedsReducer,
  cats: catsReducer,
});
