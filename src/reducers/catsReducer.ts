import { CatsAction, LoadingAction } from '../actions/catActions';
import { CatState } from '../types/cat';
import ActionTypes from '../actionTypes';
import { appendCats } from '../util/array';

const initialState = {
  list: [],
  selected: null,
  loading: false,
};

export default (
  state: CatState = initialState,
  action: CatsAction | LoadingAction
): CatState => {
  switch (action.type) {
    case ActionTypes.fetchCats:
      return {
        ...state,
        list: appendCats(state.list, action.payload),
      };
    case ActionTypes.clearCats:
      return { ...state, list: [] };
    case ActionTypes.loadingCats:
      return { ...state, loading: action.payload };
    default:
      return state;
  }
};
