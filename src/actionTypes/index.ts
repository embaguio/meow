export enum ActionTypes {
  fetchBreeds = 'fetchBreeds',
  selectBreed = 'selectBreed',
  fetchCats = 'fetchCats',
  clearCats = 'clearCats',
  loadingCats = 'loadingCats',
}

export default ActionTypes;
