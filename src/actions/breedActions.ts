import { Dispatch } from 'redux';
import catAPI from '../services/catApi';
import ActionTypes from '../actionTypes';
import { Breed } from '../types/breed';

export interface FetchBreedsAction {
  type: ActionTypes.fetchBreeds;
  payload: Array<Breed>;
}

export interface SelectBreedAction {
  type: ActionTypes.selectBreed;
  payload: Breed | null;
}

export const fetchBreeds = () => async (dispatch: Dispatch) => {
  const result = await catAPI.get<Array<Breed>>('/breeds');
  dispatch<FetchBreedsAction>({
    type: ActionTypes.fetchBreeds,
    payload: result.data,
  });
};

export const selectBreed = (breed: Breed) => {
  return {
    type: ActionTypes.selectBreed,
    payload: breed,
  };
};
