import { Dispatch } from 'redux';
import catAPI from '../services/catApi';
import ActionTypes from '../actionTypes';
import { Cat } from '../types/cat';

const LIMIT = 10;

export interface CatsAction {
  type: ActionTypes.fetchCats | ActionTypes.clearCats;
  payload: Array<Cat>;
}

export interface LoadingAction {
  type: ActionTypes.loadingCats;
  payload: boolean;
}

export const fetchCats = (breedId: string, page: number) => async (
  dispatch: Dispatch
) => {
  dispatch<LoadingAction>({
    type: ActionTypes.loadingCats,
    payload: true,
  });
  const result = await catAPI.get('/images/search', {
    params: {
      page,
      limit: LIMIT,
      breed_id: breedId,
    },
  });
  dispatch<CatsAction>({
    type: ActionTypes.fetchCats,
    payload: result.data,
  });
  dispatch<LoadingAction>({
    type: ActionTypes.loadingCats,
    payload: false,
  });
};

export const clearCats = () => {
  return {
    type: ActionTypes.clearCats,
  };
};
