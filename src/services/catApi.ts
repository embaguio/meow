import axios from 'axios';
const BASE_URL = 'https://api.thecatapi.com/v1/';

export default axios.create({
  baseURL: BASE_URL,
});

export const getCatDetail = async (id: string) => {
  const result = await axios.get(`${BASE_URL}images/${id}`);
  return result.data;
};
