import React from 'react';
import Card from 'react-bootstrap/Card';
import { Link } from 'react-router-dom';

export interface Props {
  id: string;
  url: string;
}

const CatCard = (props: Props) => {
  return (
    <Card className="card">
      <Card.Img src={props.url} />
      <Card.Body>
        <Link className="btn btn-primary btn-block" to={`/${props.id}`}>
          View details
        </Link>
      </Card.Body>
    </Card>
  );
};

export default CatCard;
