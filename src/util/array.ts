import { Breed } from '../types/breed';
import { Cat } from '../types/cat';

export const getBreed = (list: Array<Breed>, id: string) => {
  return list.find((breed: Breed) => breed.id === id) || null;
};

export const appendCats = (previousCats: Array<Cat>, newCats: Array<Cat>) => {
  const unique = newCats.filter(
    (cat: Cat) => previousCats.map((item: Cat) => item.id).indexOf(cat.id) < 0
  );
  return [...previousCats, ...unique];
};
