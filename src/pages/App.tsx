import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Container from 'react-bootstrap/Container';
import HomePage from './HomePage';
import CatPage from './CatPage';
import './styles.scss';

const App = () => {
  return (
    <div className="app-container">
      <Container>
        <Router>
          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route path="/:id" component={CatPage} />
            <Route render={() => <Redirect to="/" />} />
          </Switch>
        </Router>
      </Container>
    </div>
  );
};

export default App;
