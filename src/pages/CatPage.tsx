import React, { useState, useEffect } from 'react';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import get from 'lodash/get';
import Card from 'react-bootstrap/Card';
import Spinner from 'react-bootstrap/Spinner';
import { getCatDetail } from '../services/catApi';
import { Cat } from '../types/cat';

export default function CatPage(props: RouteComponentProps<{ id: string }>) {
  const [details, setDetails] = useState({} as Cat);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const id = props.match.params.id;
    async function fetchDetails() {
      setLoading(true);
      const result = await getCatDetail(id);
      setDetails(result);
      setLoading(false);
    }

    fetchDetails();
  }, [props.match.params.id]);

  const renderCard = () => {
    const catDetail = get(details, 'breeds[0]', null);
    return catDetail ? (
      <Card>
        <Card.Header>
          <Link className="btn btn-primary" to={`/?breed=${catDetail.id}`}>
            Back
          </Link>
        </Card.Header>
        <Card.Img src={details.url} />
        <Card.Body>
          <h4>{catDetail.name}</h4>
          <h5>Origin: {catDetail.origin}</h5>
          <h6>{catDetail.temperament}</h6>
          <p>{catDetail.description}</p>
        </Card.Body>
      </Card>
    ) : null;
  };

  return loading ? (
    <Spinner animation="border" variant="primary" />
  ) : (
    renderCard()
  );
}
