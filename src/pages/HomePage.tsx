import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import get from 'lodash/get';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import CatCard from '../components/CatCard';
import { Location } from 'history';

import { StoreState } from '../reducers';
import { fetchBreeds, selectBreed } from '../actions/breedActions';
import { fetchCats, clearCats } from '../actions/catActions';
import { Breed, BreedState } from '../types/breed';
import { Cat, CatState } from '../types/cat';
import { getBreed } from '../util/array';
import { parseQuery } from '../util/string';

interface Props {
  breeds: BreedState;
  cats: CatState;
  fetchBreeds(): any;
  selectBreed(breed: Breed | null): any;
  fetchCats(breedId: string, page: number): any;
  clearCats(): any;
  location: Location;
}

const HomePage: React.FC<Props> = (props) => {
  const {
    breeds,
    fetchBreeds,
    selectBreed,
    fetchCats,
    cats,
    clearCats,
    location,
  } = props;
  const [cardLength, setCardLength] = useState(0);
  const [page, setPage] = useState(1);
  const [hideButton, setHideButton] = useState(false);

  useEffect(() => {
    function getQuery() {
      if (!breeds.list.length) {
        fetchBreeds();
        return;
      }
      const queryString = get(location, 'search');
      if (queryString) {
        const { breed } = parseQuery(queryString);
        if (breed) onSelectBreed(breed);
      }
    }
    getQuery();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [breeds.list.length, location.search]);

  const onSelectBreed = (value: string) => {
    const selected = getBreed(breeds.list, value);
    selectBreed(selected);
    const id = get(selected, 'id', null);
    if (id) {
      const initPage = 1;
      fetchCats(id, initPage);
      setPage(initPage + 1);
    }
  };

  const onSelectChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target;
    clearCats();
    setHideButton(false);
    onSelectBreed(value);
  };

  const renderCards = (): React.ReactNode => {
    const breedList = get(breeds, 'list', []);
    return !breedList.length || !cats.list.length ? (
      <p>No cats available</p>
    ) : (
      <div className="home-row">
        <Row>
          {cats.list.map((cat: Cat) => (
            <Col md={3} sm={6} xs={12} key={cat.id}>
              {CatCard(cat)}
            </Col>
          ))}
        </Row>
      </div>
    );
  };

  const renderOptions = (): React.ReactNode => {
    const hasBreed = get(breeds, 'list.length', false);
    if (!hasBreed) return <React.Fragment />;

    const options = breeds.list.map(({ id, name }: Breed) => {
      return (
        <option key={id} value={id}>
          {name}
        </option>
      );
    });
    return options;
  };

  const loadMore = () => {
    const id = get(breeds, 'selected.id', null);
    if (id) {
      fetchCats(id, page);
      setPage(page + 1);
      cardLength >= cats.list.length && setHideButton(true);
    }
    setCardLength(cats.list.length);
  };

  const renderLoadMore = () => {
    const disableBtn: boolean =
      !breeds.list.length || !cats.list.length || cats.loading;
    const btnLabel = cats.loading ? 'Loading...' : 'Load more';
    return (
      <Row>
        <Col md={3} sm={6} xs={12}>
          <Button
            disabled={disableBtn}
            variant="success"
            type="button"
            onClick={loadMore}
          >
            {btnLabel}
          </Button>
        </Col>
      </Row>
    );
  };

  const renderFilter = () => {
    const selectedBreed = get(breeds, 'selected.id', '');
    return (
      <div>
        <Row style={{ padding: '10px 0' }}>
          <Col md={3} sm={6} xs={12}>
            <Form.Group controlId="breed">
              <Form.Label>Breed</Form.Label>
              <Form.Control
                as="select"
                disabled={!breeds.list.length}
                value={selectedBreed}
                onChange={onSelectChange}
              >
                <option value="">Select breed</option>
                {renderOptions()}
              </Form.Control>
            </Form.Group>
          </Col>
        </Row>
      </div>
    );
  };

  return (
    <React.Fragment>
      <h1>Cat Browser</h1>
      {renderFilter()}
      {renderCards()}
      {!hideButton && renderLoadMore()}
    </React.Fragment>
  );
};

const mapStateToProps = ({ breeds, cats }: StoreState) => {
  return { breeds, cats };
};

export default connect(mapStateToProps, {
  fetchBreeds,
  selectBreed,
  fetchCats,
  clearCats,
})(HomePage);
