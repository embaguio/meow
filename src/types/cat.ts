import { Breed } from './breed';

export interface Cat {
  id: string;
  url: string;
  height: number;
  width: number;
  breeds: Array<Breed>;
}

export interface CatState {
  list: Array<Cat>;
  selected: Cat | null;
  loading: boolean;
}
