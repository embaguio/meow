export interface Breed {
  id: string;
  name: string;
  origin: string;
  temperament: string;
  description: string;
}
export interface BreedState {
  list: Array<Breed>;
  selected: Breed | null;
}
